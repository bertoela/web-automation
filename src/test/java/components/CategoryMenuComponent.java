package components;

import org.openqa.selenium.WebDriver;
import pages.BasePage;

public class CategoryMenuComponent extends BasePage {

    private static final String CATEGORY_XPATH = "//ul[contains(@class, 'categories-menu')]/li/a[contains(text(),'@CATEGORY_TEXT@')]";
    private static final String CATEGORY_TEXT = "@CATEGORY_TEXT@";

    public CategoryMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void selectCategory(String text, int timeOut) {
        extractElementByText(CATEGORY_XPATH, CATEGORY_TEXT, text, timeOut, driver).click();
    }
}



