package components;

import org.openqa.selenium.WebDriver;
import pages.BasePage;

public class SubcategoryListComponent extends BasePage {

    private static final String SUBCATEGORY_XPATH = "//ul[contains(@class, 'subcategories categories-grid')]/li[contains(@class, 'subcategory category')]//div[contains(text(),'@SUBCATEGORY_TEXT@')]";
    private static final String SUBCATEGORY_TEXT = "@SUBCATEGORY_TEXT@";

    public SubcategoryListComponent(WebDriver driver) {
        super(driver);
    }

    public void selectItem(String text, int timeOut) {
        extractElementByText(SUBCATEGORY_XPATH, SUBCATEGORY_TEXT, text, timeOut, driver).click();
    }

    public void selectItem(String text) {
        extractElementByText(SUBCATEGORY_XPATH, SUBCATEGORY_TEXT, text, driver).click();
    }
}