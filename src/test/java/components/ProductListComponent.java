package components;

import org.openqa.selenium.WebDriver;
import pages.BasePage;

public class ProductListComponent extends BasePage {

    private static final String PRODUCT_XPATH = "//div[contains(@class, 'react-cell')]//div//div//button//div[contains(@title, '@PRODUCT_TEXT@')]";
    private static final String PRODUCT_TEXT = "@PRODUCT_TEXT@";

    public ProductListComponent(WebDriver driver) {
        super(driver);
    }

    public void selectProduct(String text, int timeOut) {
        extractElementByText(PRODUCT_XPATH, PRODUCT_TEXT, text, timeOut, driver).click();
    }
}