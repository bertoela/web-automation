package pages;

import org.openqa.selenium.WebDriver;

public class DeliveryPage extends BasePage {

    private static final String DELIVERY_XPATH = " //div[contains(@class,'shopping-context-container')]//*[@id='shopping-selector-shop-context-intent-@DELIVERY_NAME@']";
    private static final String DELIVERY_NAME = "@DELIVERY_NAME@";

    public DeliveryPage(WebDriver driver) {
        super(driver);
    }

    public void selectDelivery(String deliveryName, int timeOut) {
        extractElementByText(DELIVERY_XPATH, DELIVERY_NAME, deliveryName, timeOut, driver).click();
    }
}
