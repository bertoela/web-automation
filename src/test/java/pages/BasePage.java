package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class BasePage {
    protected WebDriver driver;
    private final int TIME_OUT = 5;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement extractElement(By selector, int timeOut, WebDriver driver) {
        if (timeOut <= 5) {
            timeOut = TIME_OUT;
        }
        return await().atMost(timeOut, TimeUnit.SECONDS).ignoreExceptions()
                .until(() -> driver.findElement(selector), WebElement::isDisplayed);
    }

    public WebElement extractElement(By selector, WebDriver driver) {
        return await().atMost(TIME_OUT, TimeUnit.SECONDS).ignoreExceptions()
                .until(() -> driver.findElement(selector), WebElement::isDisplayed);
    }

    public WebElement extractElementByText(String xPath, String key, String text, Integer timeOut, WebDriver driver) {
        return extractElement(getDynamicXpath(xPath, key, text), timeOut, driver);
    }

    public WebElement extractElementByText(String xPath, String key, String text, WebDriver driver) {
        return extractElement(getDynamicXpath(xPath, key, text), driver);
    }

    protected By getDynamicXpath(String xpathValue, String placeholder, String substitutionValue) {
        return By.xpath(getDynamicXpathAsString(xpathValue, placeholder, substitutionValue));
    }

    protected String getDynamicXpathAsString(String xpathValue, String placeholder, String substitutionValue) {
        return xpathValue.replace(placeholder, substitutionValue);
    }
}
