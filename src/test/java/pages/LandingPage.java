package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingPage extends BasePage {

    private final By.ByXPath BUTTON_OPEN_CART_XPATH = new By.ByXPath("//div[contains(@body-class,'nav-sticky')]//div[contains(@class,'left right-section')]//a");

    public LandingPage(WebDriver driver) {
        super(driver);
    }

    public LandingPage openLandingPage() {
        driver.get("https://www.wegmans.com/stores/");
        return this;
    }

    public void openCart(int timeOut) {
        extractElement(BUTTON_OPEN_CART_XPATH, timeOut, driver).click();
    }
}