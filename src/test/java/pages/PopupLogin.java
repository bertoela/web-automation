package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class PopupLogin extends BasePage {

    private final By.ByXPath popup = new By.ByXPath("//*[@id='SignInModal']/div");

    public PopupLogin(WebDriver driver) {
        super(driver);
    }

    public void close() {
        extractElement(popup, 5, driver).click();
    }
}
