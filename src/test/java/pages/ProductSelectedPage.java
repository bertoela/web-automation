package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductSelectedPage extends BasePage {
    private static final String MODAL_BASE_XPATH = "//div[contains(@class,'modal-content')]";
    //private static final String PRODUCT_NAME_XPATH = MODAL_BASE_XPATH + "//div[contains(@class,'product-info')]//span[(text()=@PRODUCT_TEXT@)]";
    //private static final String PRODUCT_TEXT = "@PRODUCT_TEXT@";
    private final By.ByXPath BUTTON_ADD_TO_CART_XPATH = new By.ByXPath(MODAL_BASE_XPATH + "//div[contains(@class,'product-info')]//div[contains(@class,'product-details')]//button");
    private final By.ByXPath CLOSE_XPATH = new By.ByXPath("//div[contains(@class,'product-details-modal modal-container')]//button[contains(@class,'close')]");

    public ProductSelectedPage(WebDriver driver) {
        super(driver);
    }

    public void addProductToList(String productName, int timeOut) {
        extractElement(BUTTON_ADD_TO_CART_XPATH, timeOut, driver).click();
    }

    public void close() {
        extractElement(CLOSE_XPATH, driver).click();
    }

}
