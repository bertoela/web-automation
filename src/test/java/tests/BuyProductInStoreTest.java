package tests;

import components.CategoryMenuComponent;
import components.ProductListComponent;
import components.SubcategoryListComponent;
import org.junit.jupiter.api.Test;
import pages.DeliveryPage;
import pages.LandingPage;
import pages.PopupLogin;
import pages.ProductSelectedPage;

public class BuyProductInStoreTest extends BaseTest {

    @Test
    public void BuyProductInStore() {
        LandingPage landing = new LandingPage(driver);
        landing.openLandingPage();
        PopupLogin popup = new PopupLogin(driver);
        popup.close();
        CategoryMenuComponent categoryList = new CategoryMenuComponent(driver);
        categoryList.selectCategory("Grocery", 5);
        popup.close();
        DeliveryPage deliveryList = new DeliveryPage(driver);
        deliveryList.selectDelivery("instore", 5);
        SubcategoryListComponent subcategoryList = new SubcategoryListComponent(driver);
        subcategoryList.selectItem("Beverages", 5);
        ProductListComponent productList = new ProductListComponent(driver);
        productList.selectProduct("Coca-Cola Cola", 10);
        ProductSelectedPage productSelected = new ProductSelectedPage(driver);
        productSelected.addProductToList("Coca-Cola Cola", 5);
        productSelected.close();
        landing.openCart(5);
        landing.openCart(5);
        //seleccionar el producto para recoger en tienda
        //comparar precios
        //Seleccionar compra online
        //Seleccionar tienda
        //comprobar que  sea la tienda elegida
    }
}
