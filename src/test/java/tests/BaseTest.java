package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {
    protected WebDriver driver;
    // protected NgWebDriver ngWebDriver;
    // protected JavascriptExecutor jsDriver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        // jsDriver = (JavascriptExecutor) driver;
        // ngWebDriver = new NgWebDriver(jsDriver);
        // ngWebDriver.waitForAngularRequestsToFinish();
        driver.manage().window().maximize();
    }

    @AfterEach
    public void clean() {
        driver.quit();
    }
}
